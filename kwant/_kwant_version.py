# This file will be overwritten by setup.py when a source or binary
# distribution is made.  The magic value "__use_git__" is interpreted by
# _common.py in this directory.
version = "__use_git__"

# These values are only set if the distribution was created with 'git archive'
refnames = "HEAD -> master, refs/merge-requests/408/head, refs/keep-around/7790469517c853aad89c224aeadc5f42c36efcd3"
git_hash = "779046951"
